import React from 'react'
import { FooterWrapper, FooterParagraph, FooterHeading, FooterImage, CopyRightContent, FooterLearn, DetailWrapper, FooterImageWrapper } from 'styles/components/Footer'
import MoneyBack from 'assets/Money.svg'
const Footer: React.FC = () => {
  return (

    <FooterWrapper>
      <FooterImageWrapper>
        <FooterImage src={MoneyBack} />
      </FooterImageWrapper>
      <DetailWrapper>
        <br /><br />
        <FooterHeading> Money-Back Guarantee</FooterHeading>
        <FooterParagraph>In case you do not get visible results, you can get a full refund within 60 days after purchase.</FooterParagraph>
        <FooterLearn>Learn more </FooterLearn>
        <CopyRightContent>Copyright<sup>@</sup>2023 Amomama Media Limited <br />
          All RIghts Reserved</CopyRightContent>
      </DetailWrapper>
    </FooterWrapper>
  )
}

export default Footer
