import React from 'react'
import { WhatYouGetWrapper, WhatYouGetHeading, WhatYouGetPoints, WhatYouGetPoint, Circle, Tick } from 'styles/components/WhatYouGet'
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import { faCheck } from "@fortawesome/free-solid-svg-icons"
export const WhatYouGet = () => {
    return (
        <WhatYouGetWrapper >
            <WhatYouGetHeading>What you get</WhatYouGetHeading>
            <WhatYouGetPoints>
                <WhatYouGetPoint>
                    <Circle>
                        <Tick><FontAwesomeIcon icon={faCheck} /></Tick>
                    </Circle>

                    Shred pounds fast
                </WhatYouGetPoint>
                <WhatYouGetPoint>
                    <Circle>
                        <Tick><FontAwesomeIcon icon={faCheck} /></Tick>
                    </Circle>
                    Forget about yo-yo dieting
                </WhatYouGetPoint>
                <WhatYouGetPoint>
                    <Circle>
                        <Tick><FontAwesomeIcon icon={faCheck} /></Tick>
                    </Circle>
                    Learn new eating patterns
                </WhatYouGetPoint>
                <WhatYouGetPoint>
                    <Circle>
                        <Tick><FontAwesomeIcon icon={faCheck} /></Tick>
                    </Circle>
                    Get toned and lean at home (no gym needed)
                </WhatYouGetPoint>
                <WhatYouGetPoint>
                    <Circle>
                        <Tick><FontAwesomeIcon icon={faCheck} /></Tick>
                    </Circle>
                    Aquire new habits like mindful eating
                </WhatYouGetPoint>
                <WhatYouGetPoint>
                    <Circle>
                        <Tick><FontAwesomeIcon icon={faCheck} /></Tick>
                    </Circle>
                    Deal with food cravings
                </WhatYouGetPoint>
                <WhatYouGetPoint>
                    <Circle>
                        <Tick><FontAwesomeIcon icon={faCheck} /></Tick>
                    </Circle>
                    Enjoy improved health, wellbeing and quality of life
                </WhatYouGetPoint>
            </WhatYouGetPoints>
        </WhatYouGetWrapper>
    )
}
