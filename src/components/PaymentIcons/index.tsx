import React from 'react'
import { Tabs } from 'antd';
import { PaymentIconContainer, PaypalImage, PaypalImageWrapper } from 'styles/components/PaymentIcons';
import { CardPayment } from 'components/CardPayment';
import paypal from "assets/paypal.png";
import visa from "assets/visa.png";
import TabPane from 'antd/es/tabs/TabPane';
interface TabContent {
    key: string;
    content: React.ReactNode;
    img: React.ReactNode;
}


export const PaymentIcons = () => {
    const tabContent: TabContent[] = [
        {
            key: "tab1",
            content: <CardPayment />,
            img: <PaypalImageWrapper> <PaypalImage height="45px" width="85px" src={paypal} /></PaypalImageWrapper>,
        },
        {
            key: 'tab2',
            content: <CardPayment />,
            img: <PaypalImageWrapper > <PaypalImage height="45px" width="85px" src={visa} /></PaypalImageWrapper>,

        },

    ];
    return (
        <PaymentIconContainer >
            <Tabs {...tabContent} centered onChange={(key) => {
            }}>
                {tabContent.map((tab) => (
                    <TabPane  key={tab.key} tab={tab.img}>
                        {tab.content}
                    </TabPane>
                ))}
            </Tabs>
        </PaymentIconContainer >

    )
}
