import { DownPaymentWrapper, InnerContentWrapper, InnerParagraph, ContentDiv, DownPaymentIconWrapper, GoalHeading, GoalIcon, GoalIconText, GoalIconWrapper, GoalWrapper, Divider } from "styles/components/DownPaymentSection"
import { DownPaymentButton, MainWrapper } from "styles/components/DownPaymentSection"
import { Button, ButtonWrapper } from "styles/components/Header"
import { Data } from 'components/DownPaymentSection/data'
import Modals from "components/Modal"
import { useState, useEffect } from 'react';
import InnerContents from "components/InnerContent"
import MostPolpular from 'components/MostPopular'
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import { faBullseye } from "@fortawesome/free-solid-svg-icons"
import { faClapperboard } from "@fortawesome/free-solid-svg-icons"



const DownPaymentSection = ()=> {
    const [isModalOpen, setIsModalOpen] = useState<boolean>(false);
    const [backgroundColor, setBackgroundColor] = useState<number>(0);
    const handleButtonClick = (id: number) => {
            setBackgroundColor(id)
        
    }
    useEffect(
    ()=>{},[backgroundColor])
  
    return (
        <MainWrapper>

            <DownPaymentIconWrapper>
                <GoalWrapper>
                    <GoalHeading>Goal</GoalHeading>
                    <GoalIconWrapper>
                        <GoalIcon><FontAwesomeIcon icon={faBullseye} /></GoalIcon>
                        <GoalIconText>Fat burning furnace</GoalIconText>
                    </GoalIconWrapper>
                </GoalWrapper>
                <Divider />
                <GoalWrapper>
                    <GoalHeading>Target weight</GoalHeading>
                    <GoalIconWrapper>
                        <GoalIcon><FontAwesomeIcon icon={faClapperboard} /></GoalIcon>
                        <GoalIconText>111 lb</GoalIconText>
                    </GoalIconWrapper>
                </GoalWrapper>
            </DownPaymentIconWrapper>


            {Data.map((item) => (
                <DownPaymentWrapper key={item.id}>
                    <DownPaymentButton onClick={()=>handleButtonClick(item.id)} style={{ borderColor: item.id === backgroundColor?item.borderColor:"" }}  >
                        {item.id === 3 ? <MostPolpular item={item}   backgroundColor={backgroundColor} /> : <ContentDiv style={{ display: "flex" }} 
                        ><InnerContents item={item} backgroundColor={backgroundColor}  /></ContentDiv>}

                    </DownPaymentButton>

                </DownPaymentWrapper>
            ))
            }
            <ButtonWrapper>
                <Button style={{ width: "472px", height: "56px", fontSize: "20px" }} onClick={() => setIsModalOpen(true)}> Get My Plan</Button>
            </ButtonWrapper>
            <InnerContentWrapper>
                <InnerParagraph>     Extended monthly after 1 month intro offer <br /> at full price of 29.99 USD
                </InnerParagraph>
            </InnerContentWrapper>
            {
                isModalOpen && <Modals isModalOpen={isModalOpen} setIsModalOpen={setIsModalOpen} />
            }
        </MainWrapper >
    )
}

export default DownPaymentSection