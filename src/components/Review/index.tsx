import React from 'react'
import { ReviewComponentWrapper, ReviewItem, ReviewItemInsideWrapper, ReviewItemWrapper, ReviewText, ReviewUser, UserIcon, UserInfoWrapper, UserName, UserStar, UserStarWrapper } from 'styles/components/Review'
import { WhatYouGetHeading } from 'styles/components/WhatYouGet'
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import { faStar } from "@fortawesome/free-solid-svg-icons"
import user1 from "assets/author1.png"
import user2 from "assets/author2.png"
import user3 from "assets/author3.png"
import { IReviewData } from 'interface/Review';
export const Review = () => {
    const data: IReviewData[] = [{
        userName: 'Katie McPherson',
        userIcon: user1,
        userReview: "The meals are so easy and so delicious… very similar to items I normally buy but knowing how to combine the right ingredients in the right portions to guarantee weight loss day by day is so convenient. I honestly don’t even feel like I’m trying to lose weight but the scale reminds me that I am every day! I’ve lost 15 pounds in the first month alone and have more energy than I did when I thought I was “eating healthy”. Thank you for putting this genius program together :) ",
    }, {
        userName: 'NoSugarMama',
        userIcon: user2,
        userReview: 'I’m a beginner when it comes to keto, which is why I put off starting for so long… it was just overwhelming. KetoSlim was the opposite - you guys made the transition to keto super easy and enjoyable. I’m saving soooo much money by only buying ingredients I want to eat and everything gets used up in the recipes each week! No more Uber Eats for this gal!!',
    }, {
        userName: 'Keith Elliot',
        userIcon: user3,
        userReview: 'I’ve lost a whopping 42 lbs since staring on the keto diet. I don’t think I would have stuck with it for as long as I have without the sheer convenience of this program. 1) Print weekly shopping list 2) get the ingredients 3) make the meals 4) eat and lose weight. It’s so simple but the most powerful program I’ve tried by a long shot. I’m telling everyone - you’ve finally cracked the code!',
    }];

    return (
        <ReviewComponentWrapper>
            <WhatYouGetHeading>Users love our plans</WhatYouGetHeading>
            <ReviewItemWrapper>
                {data.map((item, index) => <ReviewItemInsideWrapper key={index}>
                    <ReviewItem>
                        <ReviewUser>
                            <UserInfoWrapper>
                                <UserIcon src={item.userIcon} alt='user1' />
                                <UserName>{item.userName}</UserName>
                            </UserInfoWrapper>
                            <UserStarWrapper>
                                <UserStar>
                                    <FontAwesomeIcon icon={faStar} />
                                </UserStar>
                                <UserStar>
                                    <FontAwesomeIcon icon={faStar} />
                                </UserStar>
                                <UserStar>
                                    <FontAwesomeIcon icon={faStar} />
                                </UserStar>
                                <UserStar>
                                    <FontAwesomeIcon icon={faStar} />
                                </UserStar>
                                <UserStar>
                                    <FontAwesomeIcon icon={faStar} />
                                </UserStar>
                            </UserStarWrapper>
                        </ReviewUser>
                        <ReviewText>
                            {item.userReview}
                        </ReviewText>
                    </ReviewItem>
                </ReviewItemInsideWrapper>)}
            </ReviewItemWrapper>
        </ReviewComponentWrapper>
    )
}
