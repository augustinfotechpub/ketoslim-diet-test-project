import { UpperPaymentSectionHeading, UpperPaymentSectionImage, UpperPaymentSectionImageWrapper, UpperPaymentSectionWrapper,StarWrapper,StarImage } from 'styles/components/UpperPaymentSection'
import { IUpperPaymentSection } from "interface/UpperPaymentSection"
const UpperPaymentSection = ({ heading, img, background }: IUpperPaymentSection) => {
  return (
    <UpperPaymentSectionWrapper >
      <UpperPaymentSectionHeading>{heading}</UpperPaymentSectionHeading>
      {img && <UpperPaymentSectionImageWrapper style={{ background: background }}>
        <StarWrapper style={{}}>
        <StarImage src="https://unimeal.com/_next/static/assets/I_QIjuXFXziQ9gixBuOD9/images/final/w-stars.svg"  />
        </StarWrapper>
          <UpperPaymentSectionImage src={img} />
      </UpperPaymentSectionImageWrapper>}
    </UpperPaymentSectionWrapper>
  )
}

export default UpperPaymentSection