import { useState, useEffect } from 'react';
import {ContentWrapper,InnerContent,ContentInput,Content} from 'styles/components/DownPaymentSection'

const InnerContents = ({ item, backgroundColor }: any) => {
     const [isChecked, setIsChecked] = useState(false);

    const handleChange = () => {
        if (item.id === backgroundColor) {
        setIsChecked(true);
        }
      
    
    };
    useEffect(() => { },[isChecked]
    )
    return (

        <>
            <Content>
                <ContentWrapper>
                    <InnerContent>
                        <ContentInput type="radio" checked={ item.id === backgroundColor?isChecked:false} onChange={handleChange} />
                        {item.plan}
                    </InnerContent>
                </ContentWrapper>
            </Content>
            <Content >
                <ContentWrapper style={{ flexDirection: "column", height: "50px", gap: "10px", justifyContent: "center" }}>
                    <InnerContent style={{ fontSize: "28px", justifyContent: "center" }}>{item.subscrpition}<sup style={{ fontSize: "10px" }}>USD</sup></InnerContent>
                    <InnerContent style={{ justifyContent: "center" }}>per day</InnerContent>
                            
                </ContentWrapper>
  
            </Content>
        </>)
}


export default InnerContents