import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import { CardPaymentWrapper, PaymentText, PaymentHeading, OldPrice, PriceWrapper, FormContainer, InputWrapper, PaymentForm, PaymentInput, PaymentLabel, PaymentDetailsWrapper, PaymentButton, PaymentIcon, PaymentButtonWrapper, CreditIcon, CardIconWrapper, Span } from 'styles/components/CardPayment'
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import { faVault } from "@fortawesome/free-solid-svg-icons"
import { faCreditCard } from "@fortawesome/free-solid-svg-icons"
import { faCircleExclamation } from "@fortawesome/free-solid-svg-icons"
import { schema } from 'validation';
import {FormData} from 'interface'

export const CardPayment = () => {
     const { register, handleSubmit, formState: { errors } } = useForm<FormData>({
    resolver: yupResolver(schema),
  });

      const onSubmit = (data: any) => {
    console.log(data);
  };
    return (
        <CardPaymentWrapper>
            <PaymentText>
                <PaymentHeading>7-day plan</PaymentHeading>
                <PriceWrapper>
                    <OldPrice><s>9.99 USD</s></OldPrice>
                    <PaymentHeading> 6.99 USD</PaymentHeading>
                </PriceWrapper>
            </PaymentText>
            <PaymentForm onSubmit={handleSubmit(onSubmit)}>
                <FormContainer>
                    <InputWrapper>
                        <PaymentLabel>Credit or Debit Card Number</PaymentLabel>
                        <CardIconWrapper>
                            <CreditIcon ><FontAwesomeIcon icon={faCreditCard} /> </CreditIcon>
                            <PaymentInput
                                type="text"
                                placeholder="XXXX XXXX XXXX XXXX"
                                maxLength={19}
                                 {...register('cardNumber')}
                                style={{ width: "100%", border: "none" }}
                            />
                            
                        </CardIconWrapper>
                    </InputWrapper>
                        {errors.cardNumber && <Span>{errors.cardNumber.message}</Span>}
                    <PaymentDetailsWrapper>
                        <InputWrapper>
                            <PaymentLabel>Expiry Date</PaymentLabel>
                            <PaymentInput
                                type="text"
                                placeholder="MM/YY"
                                maxLength={7}
                                   {...register('Exp')} />
                            {errors.Exp && <Span>{errors.Exp.message}</Span>}
                        </InputWrapper>
                        <InputWrapper>
                            <PaymentLabel>CVV/CVC</PaymentLabel>
                            <CardIconWrapper>
                                <PaymentInput
                                    type="password"
                                    placeholder="123"
                                    maxLength={3}
                                       {...register('cvv')}
                                    style={{ width: "100%", border: "none" }}
                                />
                                <CreditIcon ><FontAwesomeIcon icon={faCircleExclamation} /> </CreditIcon>
                            </CardIconWrapper>
  {errors.cvv && <Span>{errors.cvv.message}</Span>}
                        </InputWrapper>
                    </PaymentDetailsWrapper>
                    <PaymentButtonWrapper>
                        <PaymentButton name="submitButton" className="submit_button" >
                            <PaymentIcon>
                                <FontAwesomeIcon icon={faVault} />
                            </PaymentIcon>
                            PAY
                        </PaymentButton>
                    </PaymentButtonWrapper>
                </FormContainer>
            </PaymentForm>
        </CardPaymentWrapper >
    )
}
