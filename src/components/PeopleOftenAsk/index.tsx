import React from 'react'
import { WhatYouGetHeading, WhatYouGetWrapper } from '../../styles/components/WhatYouGet'
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import { faQuestion } from "@fortawesome/free-solid-svg-icons"
import { FaqWrapper, FaqItem, FaqIcon, Question, FaqContent, FaqQuestion, FaqAnswer, FaqIconWrapper } from 'styles/components/PeopleOftenAsk'
import { IFaqData } from "interface/Faq"
export const PeopleOftenAsk = () => {
  const data: IFaqData[] = [{
    question: 'What if I feel hungry?',
    answer: 'The menu we offer is balanced and filling, and you should not feel hungry. our meal plans are developed by certified nutritionists so that you could reduce cravings and stay full while having a balanced and healthy diet for efficient weight loss.'
  }, {
    question: "I'm worried that I'll quit fasting halfway.",
    answer: "We totally get it, and that's why we made sure that our Keto Plans are beginner-friendly. We help you along the way with useful tips, exact timing, and complete guidence so that you can reap the most benefits with least effort."
  }, {
    question: "Can I lose weight without working out?",
    answer: "Sure thing! Nutrition is the most important element in weight loss. However if you incorporate workouts, you body will get more toned and defined. We know it's a lot to take on, and the workouts are extremly easy, suitable for first-timers, and will take up barely any time."
  }]
  return (
    <WhatYouGetWrapper>
      <WhatYouGetHeading>People often ask</WhatYouGetHeading>
      <FaqWrapper>
        {
          data && data.map((item, index) => {
            return (
              <FaqItem key={index}>
                <FaqIconWrapper>
                  <FaqIcon>
                    <Question><FontAwesomeIcon icon={faQuestion} /></Question>
                  </FaqIcon>
                </FaqIconWrapper>
                <FaqContent>
                  <FaqQuestion>{item.question}</FaqQuestion>
                  <FaqAnswer>{item.answer}</FaqAnswer>
                </FaqContent>
              </FaqItem>
            )
          })
        }
      </FaqWrapper>
    </WhatYouGetWrapper>
  )
}
