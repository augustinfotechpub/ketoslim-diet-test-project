import React from 'react'
import { FatLevelWrapperLeft, FatLevelHeading, FatLevelListWrapper, FatLevelListActive, FatLevelListNotActive, FatLevelWrapperRight } from 'styles/components/Banner'

export const FatLevel = () => {
    return (
        <React.Fragment>
            <FatLevelWrapperLeft>
                <FatLevelHeading>Fat-burning level</FatLevelHeading>
                <FatLevelListWrapper>
                    <FatLevelListActive />
                    <FatLevelListActive />
                    <FatLevelListNotActive />
                    <FatLevelListNotActive />
                    <FatLevelListNotActive />
                </FatLevelListWrapper>
            </FatLevelWrapperLeft>
            <FatLevelWrapperRight>
                <FatLevelHeading>Fat-burning level</FatLevelHeading>
                <FatLevelListWrapper>
                    <FatLevelListActive />
                    <FatLevelListActive />
                    <FatLevelListActive />
                    <FatLevelListActive />
                    <FatLevelListActive />
                </FatLevelListWrapper>
            </FatLevelWrapperRight>
        </React.Fragment>
    )
}
