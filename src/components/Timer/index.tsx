import React, { useEffect, useState } from 'react'
import { DummyText, Minutes, TimerText, TimerWrapper } from 'styles/components/Timer';

export const Timer = () => {

    const [currentTime, setCurrentTime] = useState({
        minutes: 10,
        seconds: 0
    });

    useEffect(() => {
        const countdown = setInterval(() => {
            const { minutes, seconds } = currentTime;
            if (seconds > 0) {
                setCurrentTime({
                    ...currentTime,
                    seconds: seconds - 1
                })
            }
            if (seconds === 0) {
                if (minutes === 0) {
                    clearInterval(countdown)
                } else {
                    setCurrentTime({
                        minutes: minutes - 1,
                        seconds: 59
                    })
                }
            }
        }, 1000)

        return () => {
            clearInterval(countdown);
        }
    }, [currentTime])

    return (
        <TimerWrapper><>
            {
                currentTime.minutes === 0 && currentTime.seconds === 0 ?
                    <TimerText>Offer Expired</TimerText>
                    :
                    <TimerText>{currentTime.minutes < 10 ?
                        `0${currentTime.minutes}`
                        :
                        currentTime.minutes
                    } : {currentTime.seconds < 10 ?
                        `0${currentTime.seconds}`
                        :
                        currentTime.seconds}
                    </TimerText>
            }
        </>
            <DummyText>
                <Minutes>Minutes</Minutes>
                <Minutes>Seconds</Minutes>
            </DummyText>
        </TimerWrapper>
    )
}
