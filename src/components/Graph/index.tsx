
import { AreaChart, Area, XAxis, YAxis, ResponsiveContainer } from 'recharts';


const data = [
    { name: 'Page A', uv: 3000, pv: 2400, },
    { name: 'Page B', uv: 2900, pv: 1398, },
    { name: 'Page C', uv: 2400, pv: 9800, },
    { name: 'Page D', uv: 2100, pv: 3908, },
    { name: 'Page E', uv: 2000, pv: 4800, },
    { name: 'Page F', uv: 1500, pv: 3800, },
    { name: 'Page G', uv: 1400, pv: 4300, },
];


const gradientColor = () => {
    return (
        <linearGradient id="colorUv" x1="0" y1="0" x2="1" y2="0">
            <stop offset="0" stopColor="#ff5b4e" />
            <stop offset="1" stopColor="#13bba0" />
        </linearGradient>
    );
};


const CustomDot = (props:any) => {
  const { cx, cy, r } = props;
  return (
    <circle cx={cx} cy={cy} r={r} fill="#13bba0" />
  );
};

const CustomAreaChart = () => (
    <ResponsiveContainer width="100%" height="100%">
        <AreaChart data={data}>
            <defs>{gradientColor()}</defs>
            <XAxis hide={true} />
            <YAxis hide={true} />
            <Area type="monotone" dataKey="uv" stroke="#13bba0" fill="url(#colorUv)"   dot={<CustomDot r={6} />} />
        </AreaChart>
    </ResponsiveContainer>
);

export default CustomAreaChart;
