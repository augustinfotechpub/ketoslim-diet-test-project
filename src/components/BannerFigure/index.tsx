import React from 'react'
import { BannerImage, BannerImageWrapper, BannerFigureWrapper, BannerImageBackground, BannerImageWrapperBackground } from 'styles/components/Banner'
import before from "assets/fat.svg";
import After from "assets/image.svg";
import Arrow from "assets/arrow_white.png";
export const BannerFigure = () => {
    return (
        <React.Fragment>
            <BannerFigureWrapper style={{ background: "url('...')" }}>
                <BannerImageWrapper>
                    <BannerImage src={before} alt="before" />

                </BannerImageWrapper>
                <BannerImageWrapperBackground style={{ transform: "rotate(90deg)" }}>
                    <BannerImageBackground src={Arrow} alt="arrow" />

                </BannerImageWrapperBackground>

                <BannerImageWrapper>
                    <BannerImage src={After} alt="After" />

                </BannerImageWrapper>
            </BannerFigureWrapper>
        </React.Fragment>
    )
}

