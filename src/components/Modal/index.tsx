import { Modal } from 'antd';
import { PaymentIcons } from 'components/PaymentIcons';
import UpperPaymentSection from 'components/UpperPaymentSection';
import paymentGirl from "assets/image.svg";

const Modals = ({ isModalOpen, setIsModalOpen }: { isModalOpen: boolean; setIsModalOpen: (value: boolean) => void; }) => {



    const handleCancel = () => {
        setIsModalOpen(false);
    };

    return (
        <>
            <Modal open={isModalOpen} footer={null} onCancel={handleCancel}>
                <UpperPaymentSection img={paymentGirl} background={'#9DF0E2'} heading='Select payment method' />
                <PaymentIcons />
            </Modal>
        </>
    );
}

export default Modals;