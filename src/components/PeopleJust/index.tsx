import React from 'react'
import { WhatYouGetWrapper, WhatYouGetHeading } from 'styles/components/WhatYouGet'
import before from "assets/before.png";
import after from "assets/after.png";
import { BeforeAfterWrapper, BeforeAfterItem, BeforeImageWrapper, BeforeAfterImage, ItemText, AfterImageWrapper } from 'styles/components/PeopleJust';
export const PeopleJust = () => {
    return (
        <WhatYouGetWrapper>
            <WhatYouGetHeading>People just like you achieved great results using our Weight Loss Plans</WhatYouGetHeading>
            <BeforeAfterWrapper>
                <BeforeAfterItem>
                    <BeforeImageWrapper>
                        <BeforeAfterImage src={before} />
                    </BeforeImageWrapper>
                    <ItemText>
                        Before
                    </ItemText>
                </BeforeAfterItem>
                <BeforeAfterItem>
                    <AfterImageWrapper>
                        <BeforeAfterImage src={after} />
                    </AfterImageWrapper>
                    <ItemText>
                        After
                    </ItemText>
                </BeforeAfterItem>
            </BeforeAfterWrapper>
        </WhatYouGetWrapper>
    )
}
