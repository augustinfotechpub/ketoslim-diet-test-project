import React from 'react'
import { BannerDataWrapperLeft, BannerDataHeading, BannerDataDescription, BannerDataWrapperRight } from 'styles/components/Banner'

export const BannerData = () => {
    return (
        <React.Fragment>
            <BannerDataWrapperLeft>
                <BannerDataHeading>Body fat</BannerDataHeading>
                <BannerDataDescription>
                    21-24%
                </BannerDataDescription>
            </BannerDataWrapperLeft>
            <BannerDataWrapperRight>
                <BannerDataHeading>Body fat</BannerDataHeading>
                <BannerDataDescription>
                    14-20%
                </BannerDataDescription>
            </BannerDataWrapperRight>
        </React.Fragment>
    )
}
