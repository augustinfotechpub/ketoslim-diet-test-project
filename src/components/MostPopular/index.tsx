import InnerContents from 'components/InnerContent'
import { DownPaymentButtonWrapper, ParagraphDiv } from 'styles/components/DownPaymentSection'

const MostPopular = ({ item ,backgroundColor}: any) => {
  return (
    <>
      <ParagraphDiv>Most Popular</ParagraphDiv>
      <DownPaymentButtonWrapper style={{
        display: "flex", width: "100%",
        justifyContent: "space-evenly", gap: "0px"
      }} >
        <InnerContents item={item} backgroundColor={backgroundColor}  />
      </DownPaymentButtonWrapper>
    </>
  )
}

export default MostPopular