import { Container, LogoContainer, ButtonWrapper, Logo, Button } from "styles/components/Header"
import logo from 'assets/KetoSlim-Logo.png'
import { Timer } from "components/Timer"
import { TimerButtonWrapper } from "styles/components/Timer"

export const Header = ({ scrollToMySection }: any) => {
    return (
        <Container >


            <LogoContainer>

                <Logo src={logo} />
            </LogoContainer>
            <TimerButtonWrapper>
                <Timer />
                <ButtonWrapper>
                    <Button style={{ fontSize: "20px", animation: "2s ease 0s infinite normal none running buttonAnimation", borderRadius: "8px" }} onClick={scrollToMySection} > Get My Plan</Button>
                </ButtonWrapper>
            </TimerButtonWrapper>

        </Container >
    )
}
export default Header