
import { FooterDownContainer, FooterMainContainer, FooterDownContent } from 'styles/components/FooterDownSection'

export const FooterDownSection = () => {
  return (
    <FooterMainContainer>
      <FooterDownContainer>
        <FooterDownContent>
          We've automatically applied the discount to your first subscription price. Please note that your subscription will be automatically renewed at the full price of 29.99 USD an end of the chosen subscription term. If you want to manage your subscription, you may do so via personal account on https://ketoslim.diet/</FooterDownContent>
      </FooterDownContainer>
    </FooterMainContainer>
  )
}

export default FooterDownSection