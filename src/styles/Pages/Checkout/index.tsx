import styled from 'styled-components'


export const ContentWrapper = styled.div`
    margin-left: auto;
    margin-right: auto;
    max-width: 580px;
    padding-left: 1.25rem;
    padding-right: 1.25rem;
    width: 100%;
    gap: 20px;
    display: flex;
    flex-direction: column;
@media (max-width:614px) {
max-width: 450px;}
@media (max-width:514px) {
max-width: 320px;
padding: 0;}
`