


import styled from 'styled-components'


export const UpperPaymentSectionWrapper = styled.div`

display:flex;
    justify-content:center ;
    align-items: center;
flex-direction:column;
border-radius: 10px;
`
export const UpperPaymentSectionImageWrapper = styled.div`
    width: 100%;
    display: flex;
    justify-content: center;
border-radius:15px;
`
export const StarWrapper = styled.div`
display: flex;
    align-items: center;
    height: 150px;
    width: 30px;
`
export const StarImage =styled.img``

export const UpperPaymentSectionImage = styled.img`
height:235px;
border-radius: 15px;
`


export const UpperPaymentSectionHeading = styled.h2`

    text-align:center;
    max-width:279px;
    letter-spacing: -0.3px;
    text-transform: none;
`

