import styled from 'styled-components'

export const FooterDownContainer = styled.div`

    display:flex;
    justify-content: center;
    align-items: center;
    width:100%;
    max-width:700px;
    height:200px;
`

export const FooterDownContent = styled.p`
font-weight: 400;
    font-size: 12px;
    line-height: 12px;
    text-align: center;
color:#C2C2C2;
`
export const FooterMainContainer = styled.div`

    display:flex;
    justify-content: center;
    align-items: center;

`
