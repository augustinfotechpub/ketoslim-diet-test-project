import styled from "styled-components";

export const PaymentIconContainer = styled.div`
    display: flex;
    align-items: center;
    justify-content: center;
`


export const PaypalImageWrapper = styled.div`
    border-radius: 10px;
    border : 2px solid #13BBA0;
    padding: 10px;
    
`

export const PaypalImage = styled.img`
    height: "45px";
     width: "85px";
`