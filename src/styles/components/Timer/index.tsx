import styled from "styled-components";

export const TimerText = styled.h2`
    font-weight : 800;
    color : #00857a;
    margin-bottom: 0px;
`

export const TimerWrapper = styled.div`
    display: flex;
    align-items: center;
    justify-content: center;
    min-width: 100px;
    flex-direction: column;
`

export const TimerButtonWrapper = styled.div`
    display : flex;
    justify-content:  space-between;
    align-items: center;
    padding: 0px 10px;
    width: 100%;
`

export const DummyText = styled.div`
    display: flex;
    justify-content: space-evenly;
    width: 100%;
`
export const Minutes = styled.p`
    font-size: 8px;
    font-weight: 800;
    text-transform: uppercase;
`