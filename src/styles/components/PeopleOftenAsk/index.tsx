import styled from "styled-components"

export const FaqWrapper = styled.div`
    display: flex;
    flex-direction: column;
    gap: 10px;
    
`
export const FaqItem = styled.div`
    display: flex;
    gap: 10px;
    align-items: flex-start;
    :not(:first-child){
        border-top: 2px solid #13bba0;
        padding-top: 10px;
    }
`
export const FaqIcon = styled.div`
     width: 20px;
    height: 20px;
    border-radius: 50%;
    background: white;
    border: 2px solid #13bba0;
    display: flex;
    align-items: center;
    justify-content: center;
`
export const FaqContent = styled.div`
    display: flex;
    flex-direction : column;
    gap: 5px;
    text-align:left;
`

export const Question = styled.span`
    color: #ff584b;
    font-size: 12px;
    line-height: 1;
`
export const FaqQuestion = styled.h3`
    font-weight: 500;
    margin-top: 0px;
`

export const FaqAnswer = styled.p`
    font-size: 16px;
    margin-top: 0px;
`

export const FaqIconWrapper = styled.div`
    
`
