import styled from 'styled-components'


export const WhatYouGetWrapper = styled.div`
    display: flex;
    flex-direction: column;
    gap: 10px;
`
export const WhatYouGetHeading = styled.h2`
    display : flex;
    justify-content: center;
    text-align:center;
`
export const WhatYouGetPoints = styled.ul`
    list-style-type: none;
    display:flex;
    flex-direction: column;
    gap: 10px;
    padding-left: 0px;
`
export const WhatYouGetPoint = styled.li`
    display:flex;
    align-items: center;
    gap: 10px;
`

export const Circle = styled.div`
    min-width: 20px;
    min-height: 20px;
    border-radius: 50%;
    background: #13bba0;
    display: flex;
    align-items: center;
    justify-content: center;
`
export const Tick = styled.span`
    color: white;
    font-size: 12px;
    line-height: 1;
`
