import styled from "styled-components";
import { keyframes } from 'styled-components'

export const GraphTooltip = styled.div`
    display: flex;
    align-items: center;
    justify-content: center;
    padding: 2px 10px;
    border: none;
    border-radius: 10px;
`
export const GraphContent = styled.p`
    color: white;
    font-size: 16px;
    font-weight: 500;
`
const TooltipText1Animation = keyframes`
  0% { opacity: 0; }
  50% { opacity: 0; }
  100% { opacity:1};
`

export const TooltipText1 = styled.div`
    font-size:16px;
    padding:10px 20px;
    border-radius:10px;
    border:none;
    background-color:#ff5b4e;
    display:inline-block;
    color:#fff;
    position: absolute;
    top: -40px;
    left: 30px;
    animation-name: ${TooltipText1Animation};
    animation-duration: 3s;

    @media only screen and (max-width: 614px) {
        top: -50px;
        left: 10px;
      }

      @media only screen and (max-width: 514px) {
        top: -40px;
        left: 0;
        font-size:14px;
        padding:8px 15px;
      }
`

export const TooltipText2 = styled.div`
    font-size:16px;
    padding:10px 20px;
    border-radius:10px;
    border:none;
    background-color:#13BBA0;
    display:inline-block;
    color:#fff;
    position: absolute;
    top: 140px;
    right: 28px;
    z-index:1;
    animation-name: ${TooltipText1Animation};
    animation-duration: 3s;
    @media only screen and (max-width: 614px) {
        top: 130px;
        right: 0px;
      }
      @media only screen and (max-width: 514px) {
        top: 150px;
        font-size:14px;
        right: -10px;
        padding:8px 15px;
      }
`

export const TooltipArrow1 = styled.div`
position: absolute;
top: 100%;
left: 50%;
margin-left: -5px;
border-width: 5px;
border-style: solid;
border-color: #ff5b4e transparent transparent transparent;
`
export const TooltipArrow2 = styled.div`
position: absolute;
top: 100%;
left: 50%;
margin-left: -5px;
border-width: 5px;
border-style: solid;
border-color: #13BBA0 transparent transparent transparent;
`