import styled from 'styled-components'


export const FooterImageWrapper = styled.div`
display:flex;
justify-content:center;
align-items:center;



`
export const FooterWrapper = styled.div`
display:flex;
flex-direction:column;
justify-content:center;
align-items:center;
background:#EBF7F6;
gap:50px;
position:relative;
    @media(max-width: 521px){
        padding-top: 150 px;
    }
`

export const FooterHeading = styled.h1`
color:#292929;
font-size: 34px;
font-weight: 700;
letter-spacing: -0.2px;
line-height: 44px;
text-transform: none;
`

export const FooterParagraph = styled.p`
color:#292929;
font-size: 18px;
    font-weight: 400;
    letter-spacing: normal;
    line-height: 26px;
    text-transform: none;
    max-width: 500px;

`

export const FooterLearn = styled.a`
color:#00857A;
font-size:15px;
`
export const CopyRightContent = styled.p`
    font-size: 12px;
    line-height: 18px;
    text-align: center;
color:#5C5C5C;

`
export const FooterImage = styled.img`
position:absolute;
top:-55px;
height:137px;
width:117px;
`
export const DetailWrapper = styled.div`
display:flex;
flex-direction:column;
justify-content:center;
align-items:center;
background:#EBF7F6;
text-align: center;
`




