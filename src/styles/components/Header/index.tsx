import styled from 'styled-components'

export const Container = styled.div`
    display:flex;
    justify-content: space-around;
    align-items: center;
    gap: 10px;
    flex-wrap: wrap;
    padding-top: 10px;
`


export const LogoContainer = styled.div`

    color: #292929;
    font-size: 16px;
    line-height: 1.5;


`


export const Button = styled.button`
@keyframes buttonAnimation{
  0% { box-shadow: rgb(0, 133, 122) 0px 0px 0px 0px; }
  100% {     box-shadow: transparent 0px 0px 0px 14px;};
}
width:416px;
display:flex;
align-items: center;
border-radius: 8px;
cursor:pointer;
background:#00857a;
justify-content: center;
color:#ffff;
border:none;
padding: 10px 14px;
line-height: 28px;
font-size:18px;
    @media (max-width : 520px){
        width: 180px !important;
    }
    @media (max-width : 640px) {
        width: 320px ;
    }

`


export const Logo = styled.img`
height:35px;
width:216px;

`


export const ButtonWrapper = styled.div`
    display: flex;
    align-items: center;

    font-weight: 500;
    justify-content: center;
    letter-spacing: 0.14px;



`
export const DownButton = styled.button`
    
`

