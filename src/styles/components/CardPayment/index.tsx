import styled from "styled-components";

export const CardPaymentWrapper = styled.div`
    display: flex;
    gap: 10px;
    justify-content: center;
    flex-direction: column;
`
export const PaymentText = styled.div`
    display: flex;
    flex-direction : column;
    justify-content: center;

`

export const PaymentHeading = styled.p`
    font-size: 14px;
    font-weight: 500;
    text-align: center;
    margin-bottom: 0;
`

export const OldPrice = styled.p`
    font-size: 14px;
    font-weight: 300;

`

export const PriceWrapper = styled.div`
    display: flex;
    gap: 10px;
    justify-content: center;
`

export const PaymentForm = styled.form`
    display: flex;
    gap: 10px;
    padding:10px 0px;
`

export const FormContainer = styled.div`
    
`

export const InputWrapper = styled.div`
    display: flex;
    border-radius: 10px;
    border: border: 3px solid rgba(51,58,73,.2);
  
    flex-direction: column;
    padding-bottom: 10px;
`

export const PaymentLabel = styled.label`
    font-weight: 500;
`
export const CreditIcon = styled.span`
    height: 20px;
    width: 20px;
`

export const PaymentInput = styled.input`
    padding: 0.1em 1em 0;
    border-radius: 10px;
    border: 3px solid rgba(51, 58, 73, 0.2);
    color: #01132c;
    font-size: 1em;
    height: 3.3em;
outline:none;
`

export const CardIconWrapper = styled.div`
    padding: 0px 10px;
    display: flex;
    flex-direction: row;
    justify-content: center;
    border: 3px solid rgba(51, 58, 73, 0.2);
 border-radius: 10px;
    align-items: center;
`

export const PaymentDetailsWrapper = styled.div`
    display: flex;
    justify-content: space-between;
    gap: 20px;
    flex-wrap: wrap;
`
export const Span = styled.span`
   color:red;
`

export const PaymentButton = styled.button`
    padding: 0.8em 1.5em;
    font-weight: 700;
    border: none;
    outline: none;
    border-radius: 10px;
    background-color: #13BBA0;
    color: #fff;
    text-transform: uppercase;
    cursor: pointer;
    display: flex;
    align-items: center;
    justify-content: center;
    gap: 10px;
    width: 100%;
    font-size: 16px;
`

export const PaymentButtonWrapper = styled.div`
    display: flex;
    justify-content:center;
    align-items: center;
    max-width: 100%
    padding-top: 30px;
    animation :2s ease 0s infinite normal none running pmnRC;
`

export const PaymentIcon = styled.span``