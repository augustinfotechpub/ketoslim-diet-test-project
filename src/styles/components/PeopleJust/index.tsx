import styled from "styled-components"


export const BeforeAfterWrapper = styled.div`
    display: flex;
    justify-content: center;
    gap:13px;
`
export const BeforeAfterItem = styled.div`
    display: flex;
    flex-direction: column;
    gap: 20px;
`

export const BeforeImageWrapper = styled.div`
    border-radius: 25px;
    padding: 10px;
    transform: rotate(355deg);
    box-shadow: -10px 15px 10px 0px rgba(230,228,228,0.58);
`

export const AfterImageWrapper = styled.div`
    border-radius: 25px;
    padding:10px;
    transform: rotate(3deg);
    box-shadow: 10px 15px 10px 0px rgba(230,228,228,0.58);
`
export const BeforeAfterImage = styled.img`
    height : 234px;
    border-radius: 25px;
    overflow: hidden;
`

export const ItemText = styled.h4`
    font-weight: 400;
    text-align: center;
`