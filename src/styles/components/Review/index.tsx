import styled from "styled-components";

export const ReviewComponentWrapper = styled.div`
    display: flex;
    flex-direction: column;
    gap: 10px;
    background : #f5f5f5;
    
`

export const ReviewItemWrapper = styled.div`
    display: flex;
    gap: 10px;
   
    align-items: center;
    justify-content: center;
    flex-wrap: wrap;
    padding: 10px;
`
export const ReviewItemInsideWrapper = styled.div`
    background: white;
    border-radius: 10px;
`
export const ReviewItem = styled.div`
    display: flex;
    flex-direction: column;
    gap: 10px;
    padding: 10px;
    width: 230px;
    min-height: 300px;

`

export const ReviewUser = styled.div`
    display: flex;
    justify-content: space-between;
`

export const UserInfoWrapper = styled.div`
    display: flex;
    gap: 5px;
`

export const UserIcon = styled.img`
    height: 30px;
    weight: 30px;
    border-radius: 50%;
`

export const UserName = styled.h4`
    margin-top: 0px;
    font-size: 14px;
    font-weight: 500;
`

export const UserStarWrapper = styled.div`
    display: flex;
    gap: 2px;
    padding-top: 5px;
`

export const UserStar = styled.div`
    color : #ffff00;
    font-size: 10px;
`

export const ReviewText = styled.p`
    margin-top: 0px;
    font-size: 12px;   
`