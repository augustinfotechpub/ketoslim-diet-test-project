import styled from 'styled-components'

export const BannerWrapper = styled.div`
    position: relative;
    overflow: hidden;
    background-color: rgb(250, 242, 232);
    color: #292929;
    border-radius: 8px;
`

export const BannerContentWrapper = styled.div`
    display: grid;
    grid-template-columns: repeat(2, 1fr);
    grid-template-rows: repeat(4, auto);
`

export const TextParaLeft = styled.p`
    text-align: center;
    padding: 11px 0px;
    margin: 6px 0px;
    border-right: 2px solid rgb(255, 255, 255);
    grid-area: 1 / 1 / 2 / 2;
    font-weight : 600;
    padding-right: 35px;
  @media(max-width: 612px){
   padding: 11px 70px 11px 0px
    }
`
export const TextParaRight = styled.p`
    text-align: center;
    padding: 11px 0px;
    margin: 6px 0px;
    border-right: 2px solid rgb(255, 255, 255);
    grid-area: 1 / 3 / 2 / 2;
    font-weight : 600;
    padding-left: 35px;
  @media(max-width: 612px){
   padding: 11px 0px 11px 71px

    }
`
export const BannerFigureWrapper = styled.div`
    height: 234px;
    grid-area: 2 / 1 / 3 / 3;
    display: flex;
    justify-content:center;
`
export const BannerImageWrapper = styled.div`
    flex-shrink: 0;
    @media(max-width: 521px){
        :nth-child(2){
            transform: scale(0.5) rotate(90deg) !important;
        }
    }
`
export const BannerImage = styled.img`
    height: 234px;   
width:150px;
   @media(max-width: 612px){
       width:130px;
    }

`
export const BannerImageWrapperBackground = styled.div`
    flex-shrink: 0;
    display: flex;
    justify-content: center;
    align-items: center;
    height: 100%;
   
`
export const BannerImageBackground = styled.img`
    height: 172px;    
    @media(max-width: 521px){
       height: 80px;
       width:80px;
    }
`

export const BannerDataWrapperLeft = styled.div`
    margin: 8px 0px 0px 16px;
    padding: 8px 0px 24px;
    border-right: 2px solid rgb(255, 255, 255);
    border-bottom: 2px solid rgb(255, 255, 255);
    grid-area: 3 / 1 / 4 / 2;

`

export const BannerDataWrapperRight = styled.div`
    margin: 8px 16px 0px 0px;
    padding: 8px 0px 24px 16px;
    border-bottom: 2px solid rgb(255, 255, 255);
    grid-area: 3 / 2 / 4 / 3;

`

export const BannerDataHeading = styled.p`
    margin-bottom: 4px;
    font-weight: 600;
    font-size: 18px;
    line-height: 26px;
`

export const BannerDataDescription = styled.p`
    margin-bottom: 0px;
    font-weight: 400;
    font-size: 16px;
    line-height: 24px;
`
export const FatLevelWrapperLeft = styled.div`
    margin: 0px 0px 8px;
    padding: 16px 16px 38px;
    border-right: 2px solid rgb(255, 255, 255);
    grid-area: 4 / 1 / 5 / 2;
`
export const FatLevelWrapperRight = styled.div`
    margin: 0px 0px 8px;
    padding: 16px 16px 38px;
    grid-area: 4 / 2 / 5 / 3;
`

export const FatLevelHeading = styled.p`
    margin-bottom: 8px;
    font-weight: 600;
    font-size: 18px;
    line-height: 26px;
`

export const FatLevelDescription = styled.p`
    margin-bottom: 8px;
    font-weight: 600;
    font-size: 18px;
    line-height: 26px;
`
export const FatLevelListWrapper = styled.ul`
    display: flex;
    list-style: none;
    padding: 0px;
`
export const FatLevelListActive = styled.li`
    width: 20%;
    height: 6px;
    background-color: #33b3a8;
    :first-child {
    border-radius: 6px 0px 0px 6px;
    }
    :not(:last-child) {
    margin-right: 4px;
    }
    :last-child {
    border-radius: 0px 6px 6px 0px;
    }
`

export const FatLevelListNotActive = styled.li`
    width: 20%;
    height: 6px;
    background-color: #ffffff;
    :first-child {
    border-radius: 6px 0px 0px 6px;
    }
    :not(:last-child) {
    margin-right: 4px;
    }
    :last-child {
    border-radius: 0px 6px 6px 0px;
    }
`



