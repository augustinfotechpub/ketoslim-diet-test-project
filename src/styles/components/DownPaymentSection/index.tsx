import styled from 'styled-components'


export const MainWrapper = styled.div`

    display: flex;
 justify-content: center;
align-items:center;
    flex-direction: column;
gap:20px;


`
export const ContentDiv = styled.div`

    display: flex;

gap:22px;
    @media(max-width: 521px){
           gap: 15px;
    }

`
export const ParagraphDiv = styled.div`
margin 0;
background: #13BBA0;
width:100%;
height:20px;
border-radius: 5px;
`

export const DownPaymentWrapper = styled.div`
position: relative;
    display: flex;
max-width:472px;
min-height:64px;
    flex-direction: column;
    width: 100%;
    border-radius: 8px;
background:#FFFFFF;
gap:30px;
`

export const DownPaymentButtonWrapper = styled.div`
    display: flex;
flex-direction:column
    align-items: center;
    gap: 50px;
    justify-content: space-around;
width:100%;
border:none;
background:#ffff;
cursor:pointer;

`

export const DownPaymentButton = styled.button`
    display: flex;
    -webkit-box-pack: justify;
    flex-direction: column;
 justify-content: center;
    -webkit-box-align: center;
    align-items: center;
    border-radius: 8px;
    color: #292929;
    position: relative;
    z-index: 1;
    height: 90px;
    border: 2px solid #8F8F8F;
background:#FFFF;
cursor:pointer;
margin: 0px;
padding: 0px 0px 3px 0px;
gap: 5px;
`

export const Content = styled.div`

display:flex;
   

`
export const InnerParagraph = styled.p`

color:#CCCAC6;
text-align:center;
`
export const InnerContentWrapper = styled.div`

    background:#FFFFFF;
    display: flex;
    -webkit-box-pack: justify;
    justify-content: center;
    -webkit-box-align: center;
    align-items: center;
max-width:1400px;
width:100%;
`

export const ContentWrapper = styled.div`

display: flex;
    flex-direction: row;
    -webkit-box-align: center;
    align-items: center;
    font-weight: 500;
    font-size: 14px;
    line-height: 18px;

    white-space: nowrap;
`

export const InnerContent = styled.div`

    display: flex;
    -webkit-box-align: center;
    align-items: center; 
width:200px; 
    height: 60px;
    font-size: 20px;
    font-weight: 700;
    @media (max-width: 521px){
        font-size: 12px !important;
width:130px;
sup{
 font-size: 7px !important;
}
    }

`

export const ContentInput = styled.input`


    display: inline-block;
height:100%;
    width: 20px;
    min-height: 20px;
    background-clip: content-box;
    border: 2px solid black;
    border-radius: 50%;
    margin-right: 10px;
    flex-shrink: 0 !important;
accent-color:#00857A;
`

export const DownPaymentIconWrapper = styled.div`
    display: flex;
    padding: 10px;
    justify-content: space-evenly;
    align-items: center;
    width: 100%;
    max-width: 520px;
@media(max-width:588px) {
padding:0;}
`

export const GoalWrapper = styled.div`
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
    padding: 10px;
max-width:300px;
width:100%;
@media(max-width:588px) {
padding:0;}
`

export const Divider = styled.div`
    height: 80px;
    width: 1px;
    background : #ececec;
`

export const GoalHeading = styled.p`
    font-size: 16px;
    font-wright: 200px;
`
export const GoalIconWrapper = styled.div`
    display: flex;
    gap: 10px;
    padding: 0px 5px;

    height: 100px;
    justify-content: center;
    align-items: center;
@media(max-width:588px) {
padding:0;}
`
export const GoalIcon = styled.span`
    color: #13BBA0;
    font-size: 16px;
`
export const GoalIconText = styled.h4`
    font-size : 18px;
    font-weight: 500;

`