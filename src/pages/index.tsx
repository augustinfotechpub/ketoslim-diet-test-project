import Header from 'components/Header';
import Footer from 'views/Footer';
import { ContentWrapper } from 'styles/Pages/Checkout';
import Banner from 'views/Banner';
import { Content } from 'views/Content';
import UpperPaymentSection from 'components/UpperPaymentSection';
import DownPaymentSection from 'components/DownPaymentSection';
import { Review } from 'components/Review';
import { useEffect } from 'react';


export const CheckoutPage = () => {
  const  scrollToMySection=()=> {
  window.scrollTo(0, 800)
  }
    const heading1 = 'Your personal Keto Diet plan is ready!';
    const heading2 = 'Get visible results in 4 weeks';
    useEffect(() => {
  window.scrollTo(0, 0)
}, [])
    return (
        <ContentWrapper>
            <Header scrollToMySection={scrollToMySection} />
            <Banner />
            <UpperPaymentSection heading={heading1} />
            <DownPaymentSection />
            <Content />
            <Review />
            <UpperPaymentSection heading={heading2} />
            <DownPaymentSection />
            <br /><br />
            <Footer />
        </ContentWrapper>

    )
}



export default CheckoutPage;