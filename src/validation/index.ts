import * as yup from 'yup'
export const schema = yup.object().shape({
  cardNumber: yup
    .string()
    .required('cardNumber is required')
    .matches(/^\S/, 'First character cannot be Space ')
        .max(12),
    Exp: yup
    .string()
    .required('ExpireDate is required'),
    cvv: yup
    .string()
    .required('cvv is required')
    .max(4),
})
