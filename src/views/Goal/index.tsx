import React from 'react'
import { ContentWrapper } from 'styles/Pages/Checkout'
import CustomAreaChart from 'components/Graph'
import { ReviewComponentWrapper } from 'styles/components/Review'
import { WhatYouGetHeading } from 'styles/components/WhatYouGet'
import { Button, ButtonWrapper, Logo, LogoContainer } from 'styles/components/Header'
import { useNavigate } from 'react-router-dom'
import { TooltipText1, TooltipText2, TooltipArrow1,TooltipArrow2 } from 'styles/components/Graph';

export const Goal = () => {
    const navigate = useNavigate();
    return (
        <ContentWrapper>
            <ReviewComponentWrapper style={{ background: "#ffffff " }}>
                <LogoContainer style={{ display: "flex", justifyContent: "center", paddingTop: "30px" }}>

                    <Logo src={"https://ketoslim.diet/quiz/images/keto-logo.png"} />
                </LogoContainer>
                <WhatYouGetHeading>Your 4-week Keto Diet plan is ready!</WhatYouGetHeading>
                <div style={{ width: "100%", height: "400px",position:"relative", marginTop:"40px" }}>
                    <TooltipText1 className='tooltip-1'>Your weight <TooltipArrow1></TooltipArrow1></TooltipText1>
                    <TooltipText2 className='tooltip-2'>After 4 weeks<TooltipArrow2></TooltipArrow2></TooltipText2>
                    <CustomAreaChart />
                </div>
                <ButtonWrapper style={{ paddingBottom: "40px" }}>
                    <Button style={{ fontSize: "20px",animation:"none" }} onClick={() => {
                        navigate("/checkout");
                    }}>Continue</Button>
                </ButtonWrapper>
            </ReviewComponentWrapper>
        </ContentWrapper >
    )
}
