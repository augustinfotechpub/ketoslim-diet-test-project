import { BannerData } from 'components/BannerData'
import { FatLevel } from 'components/FatLevel'
import { BannerFigure } from 'components/BannerFigure'
import React from 'react'
import {
    BannerWrapper,
    BannerContentWrapper,
    TextParaLeft,
    TextParaRight,
} from 'styles/components/Banner'



const Banner: React.FC = () => {
    return (
        <BannerWrapper>
            <BannerContentWrapper>
                <TextParaLeft>Your Weight</TextParaLeft>
                <TextParaRight>Goal</TextParaRight>
                <BannerFigure />
                <BannerData />
                <FatLevel />
            </BannerContentWrapper>
        </BannerWrapper>
    )
}

export default Banner
