import { PeopleJust } from 'components/PeopleJust'
import { PeopleOftenAsk } from 'components/PeopleOftenAsk'
import { WhatYouGet } from 'components/WhatYouGet'
import React from 'react'

export const Content = () => {
    return (
        <React.Fragment>
            <WhatYouGet />
            <PeopleJust />
            <PeopleOftenAsk />
        </React.Fragment>
    )
}
