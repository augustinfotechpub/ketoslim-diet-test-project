
import './App.css';
import { BrowserRouter as Router, Route, Routes } from "react-router-dom";
import { Goal } from 'views/Goal';
import CheckoutPage from 'pages';


const App = () => {
  return (
    <Router>
      <Routes>
        <Route path="/checkout" Component={CheckoutPage} />
        <Route path="/" Component={Goal} />
      </Routes>
    </Router>

  );
}

export default App;
