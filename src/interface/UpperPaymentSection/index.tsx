export interface IUpperPaymentSection {
    heading: string,
    img?: string,
    background?: string
}