export interface IReviewData {
    userName: string,
    userIcon: string,
    userReview: string
}