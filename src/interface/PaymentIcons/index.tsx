export interface IPaymentIcon {
    direction: string,
    icons: [string]
}